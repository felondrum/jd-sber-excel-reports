package ru.sber.jd.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ResponseStatisticPercentDto {
    private String name;
    private Double percent;
}
