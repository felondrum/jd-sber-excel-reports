package ru.sber.jd.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.kafka.common.protocol.types.Field;

@Data
@Accessors(chain = true)
public class ClientAuthorizationDto {
    private String login;
    private String userGroup;
    private String email;
}
