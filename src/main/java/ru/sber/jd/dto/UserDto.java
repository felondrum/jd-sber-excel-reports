package ru.sber.jd.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDto {
    private Integer id;
    private String name;
    private Integer grade;
    private Integer salary;
}
