package ru.sber.jd.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ClientAuthorizationResultDto {
    private String login;
    private String userGroup;
    private String email;
    private String message;
    private String status;
}
