package ru.sber.jd.enities;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserEntity {
    private Integer id;
    private String name;
    private Integer grade;
    private Integer salary;
}
