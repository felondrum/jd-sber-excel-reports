package ru.sber.jd.fakes;

import lombok.RequiredArgsConstructor;
import ru.sber.jd.enities.ResponseStatisticEntity;
import ru.sber.jd.repositories.ResponseStatisticRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ResponseStatisticFake {
    private final ResponseStatisticRepository responseStatisticRepository;

    public void init() throws SQLException {
        List<ResponseStatisticEntity> entities = new ArrayList<>();

        ResponseStatisticEntity entity = new ResponseStatisticEntity().setId(1).setName("get-all-users").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(2).setName("get-all-users-download").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(3).setName("get-user-by-name").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(4).setName("get-user-by-grade").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(5).setName("get-user-by-salary").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(6).setName("get-user-by-grade-bound").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(7).setName("get-user-by-salary-bound").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(8).setName("add-user").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(9).setName("get-user-by-grade-bound-download").setCount(0);
        entities.add(entity);
        entity = new ResponseStatisticEntity().setId(10).setName("get-user-by-salary-bound-download").setCount(0);
        entities.add(entity);

        responseStatisticRepository.createTable();
        responseStatisticRepository.insertStats(entities);

    }

}
