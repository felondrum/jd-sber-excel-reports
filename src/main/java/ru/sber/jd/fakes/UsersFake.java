package ru.sber.jd.fakes;

import lombok.RequiredArgsConstructor;
import ru.sber.jd.enities.UserEntity;
import ru.sber.jd.repositories.UserRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public class UsersFake {
    private final UserRepository userRepository ;

    public void init() throws SQLException {

        List<UserEntity> entities = new ArrayList<>();

        for (int i = 0; i < 40; i++) {
            Integer grade = Math.abs(new Random().nextInt() % 15 + 1);
            Integer salary = grade * 12000;
            Integer id = i;
            String name = String.format("Employer%s", id);
            UserEntity userEntity = new UserEntity().setId(id).setName(name).setSalary(salary).setGrade(grade);
            entities.add(userEntity);
        }


        userRepository.createTable();
        userRepository.insertUsers(entities);

    }

}
