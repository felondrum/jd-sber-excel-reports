package ru.sber.jd.servicies;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.UserDto;
import ru.sber.jd.dto.UserNoSalaryDto;
import ru.sber.jd.enities.UserEntity;
import ru.sber.jd.repositories.UserRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;


    public List<UserDto> getAll () throws SQLException {
        return userRepository.selectAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<UserNoSalaryDto> getAllNoSalary () throws SQLException {
        return userRepository.selectAll().stream().map(this::mapToDtoNoSalary).collect(Collectors.toList());
    }

    public List<UserDto> getByName(String name) throws SQLException {
        return userRepository.selectByName(name).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<UserNoSalaryDto> getByNameNoSalary(String name) throws SQLException {
        return userRepository.selectByName(name).stream().map(this::mapToDtoNoSalary).collect(Collectors.toList());
    }

    public List<UserDto> getByGrade(Integer grade) throws SQLException {
        return userRepository.selectByGrade(grade).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<UserNoSalaryDto> getByGradeNoSalary(Integer grade) throws SQLException {
        return userRepository.selectByGrade(grade).stream().map(this::mapToDtoNoSalary).collect(Collectors.toList());
    }

    public List<UserDto> getBySalary(Integer salary) throws SQLException {
        return userRepository.selectBySalary(salary).stream().map(this::mapToDto).collect(Collectors.toList());
    }


    public List<UserDto> getByGradeBetween(Integer gradeLow, Integer gradeHigh) throws SQLException {
        return userRepository.selectByGradeBetween(gradeLow, gradeHigh).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<UserNoSalaryDto> getByGradeBetweenNoSalary(Integer gradeLow, Integer gradeHigh) throws SQLException {
        return userRepository.selectByGradeBetween(gradeLow, gradeHigh).stream().map(this::mapToDtoNoSalary).collect(Collectors.toList());
    }

    public List<UserDto> getBySalaryBetween(Integer salaryLow, Integer salaryHigh) throws SQLException {
        return userRepository.selectBySalaryBetween(salaryLow, salaryHigh).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public boolean insertUser(UserDto userDto) {
        UserEntity entity = mapToEntity(userDto);
        try {
            userRepository.insertUser(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean insertUsers(List<UserDto> userDtos) {
        List<UserEntity> entities = new ArrayList<>();
        for (UserDto dto:userDtos) {
            entities.add(mapToEntity(dto));
        }
        try {
            userRepository.insertUsers(entities);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    private UserEntity mapToEntity(UserDto dto) {
        return new UserEntity().setId(dto.getId()).setName(dto.getName()).setGrade(dto.getGrade()).setSalary(dto.getSalary());
    }



    private UserDto mapToDto(UserEntity entity) {
        return new UserDto().setId(entity.getId()).setName(entity.getName()).setGrade(entity.getGrade()).setSalary(entity.getSalary());
    }

    private UserNoSalaryDto mapToDtoNoSalary(UserEntity entity) {
        return new UserNoSalaryDto().setId(entity.getId()).setName(entity.getName()).setGrade(entity.getGrade());
    }
}
