package ru.sber.jd.servicies;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.ResponseStatisticDto;
import ru.sber.jd.dto.ResponseStatisticPercentDto;
import ru.sber.jd.dto.UserDto;
import ru.sber.jd.enities.ResponseStatisticEntity;
import ru.sber.jd.enities.UserEntity;
import ru.sber.jd.repositories.ResponseStatisticRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ResponseStatisticService {
    private final ResponseStatisticRepository responseStatisticRepository;


    public List<ResponseStatisticDto> getAll () throws SQLException {
        return responseStatisticRepository.selectAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<ResponseStatisticDto> getByName(String name) throws SQLException {
        return responseStatisticRepository.selectByName(name).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<ResponseStatisticPercentDto> getAllByPercent() throws SQLException {
        return new ArrayList<>(responseStatisticRepository.selectAllPercent());
    }

    private ResponseStatisticEntity mapToEntity(ResponseStatisticDto dto) {
        return new ResponseStatisticEntity().setId(dto.getId()).setName(dto.getName()).setCount(dto.getCount());
    }

    private ResponseStatisticDto mapToDto(ResponseStatisticEntity entity) {
        return new ResponseStatisticDto().setId(entity.getId()).setName(entity.getName()).setCount(entity.getCount());
    }

    public boolean insertStat(ResponseStatisticDto dto) {
        ResponseStatisticEntity entity = mapToEntity(dto);
        try {
            responseStatisticRepository.insertStat(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean insertStats(List<ResponseStatisticDto> dtos) {
        List<ResponseStatisticEntity> entities = new ArrayList<>();
        for (ResponseStatisticDto dto:dtos) {
            entities.add(mapToEntity(dto));
        }
        try {
            responseStatisticRepository.insertStats(entities);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateStat(String name) {
        try {
            responseStatisticRepository.updateStatByName(name);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
