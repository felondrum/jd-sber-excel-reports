package ru.sber.jd.servicies;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.sber.jd.dto.ClientAuthorizationResultDto;
import ru.sber.jd.dto.ClientAuthorizationDto;

import javax.servlet.http.Cookie;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper objectMapper = new ObjectMapper();


    public ClientAuthorizationResultDto getAuthCheck(Cookie cookie) {
        String url = "http://localhost:8082/auth/foreign";
        ClientAuthorizationDto clientAuthorizationDto;
        ClientAuthorizationResultDto clientAuthorizationResultDto = new ClientAuthorizationResultDto();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Cookie", cookie.getName() + "=" + cookie.getValue());
            ResponseEntity<String> responseEntity = restTemplate.exchange(url,
                    HttpMethod.GET,
                    new HttpEntity<String>(headers),
                    String.class);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                clientAuthorizationDto = objectMapper.readValue(responseEntity.getBody(), ClientAuthorizationDto.class);

                clientAuthorizationResultDto
                        .setLogin(clientAuthorizationDto.getLogin())
                        .setEmail(clientAuthorizationDto.getEmail())
                        .setUserGroup(clientAuthorizationDto.getUserGroup())
                        .setMessage("-")
                        .setStatus("success");
            } else {
                clientAuthorizationResultDto.setLogin("UNAUTHORIZED").setUserGroup("NONE").setEmail("NONE").setMessage(responseEntity.getBody()).setStatus("failure");
            }
            return clientAuthorizationResultDto;
        } catch (Exception e) {
            return clientAuthorizationResultDto.setLogin("UNAUTHORIZED").setUserGroup("NONE").setEmail("NONE").setStatus("failure").setMessage("Необходима авторизация!");
        }

    }


}
