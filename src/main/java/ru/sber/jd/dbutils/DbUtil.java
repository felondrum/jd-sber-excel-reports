package ru.sber.jd.dbutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {
    public static Connection createConnection() {
        try {
            return DriverManager.getConnection("jdbc:h2:~/sql;MODE=PostgreSQL", "sa", "");
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
