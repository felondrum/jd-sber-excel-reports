package ru.sber.jd.controllers;



import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ru.sber.jd.*;
import ru.sber.jd.dbutils.MediaTypeUtils;
import ru.sber.jd.dto.ClientAuthorizationResultDto;
import ru.sber.jd.dto.UserDto;
import ru.sber.jd.dto.UserNoSalaryDto;
import ru.sber.jd.exceptions.AuthorizationFailure;
import ru.sber.jd.exceptions.DataBaseUnavailableException;
import ru.sber.jd.exceptions.ExcelProblemException;
import ru.sber.jd.exceptions.NotFoundException;
import ru.sber.jd.fakes.ResponseStatisticFake;
import ru.sber.jd.fakes.UsersFake;
import ru.sber.jd.repositories.ResponseStatisticRepository;
import ru.sber.jd.repositories.UserRepository;
import ru.sber.jd.servicies.ResponseStatisticService;
import ru.sber.jd.servicies.UserService;
import ru.sber.jd.servicies.AuthService;


import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import java.sql.SQLException;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final ResponseStatisticService responseStatisticService;
    private final UserRepository userRepository;
    private final ResponseStatisticRepository responseStatisticRepository;
    private final AuthService authService;

    private final RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ServletContext servletContext;

    @Resource
    @Qualifier(value = "loggerToKafkaUserController" )
    private LoggerToKafka log;

    @CrossOrigin("*")
    @GetMapping
    public ResponseEntity findAll(@CookieValue(value = "authorization_token", required = false) Cookie cookie) throws SQLException {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-all-users");
        responseStatisticService.updateStat("get-all-users");
        try {
            if (auth.getStatus().equals("failure")) throw new NotFoundException(auth.getMessage());
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> all = userService.getAll();
                if (all.isEmpty()) {
                    throw new NotFoundException("Данные по сотрудникам отсутствуют!");
                }
                return ResponseEntity.ok(all);
            } else if (auth.getUserGroup().equals("LAZY_USER")) {
                List<UserNoSalaryDto> allNoSalary = userService.getAllNoSalary();
                if (allNoSalary.isEmpty()) {
                    throw new NotFoundException("Данные по сотрудникам отсутствуют!");
                }
                return ResponseEntity.ok(allNoSalary);
            } else {
                throw new AuthorizationFailure("Данные недоступны!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/{name}")
    public ResponseEntity findByName(@PathVariable String name, @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-name: %s", name));
        responseStatisticService.updateStat("get-user-by-name");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> byName = userService.getByName(name);
                if (byName.isEmpty()) {
                    throw new NotFoundException(String.format("Не найден сотрудник с именем %s", name));
                }
                return ResponseEntity.ok(byName);
            } else if (auth.getUserGroup().equals("LAZY_USER")) {
                List<UserNoSalaryDto> byName = userService.getByNameNoSalary(name);
                if (byName.isEmpty()) {
                    throw new NotFoundException(String.format("Не найден сотрудник с именем %s", name));
                }
                return ResponseEntity.ok(byName);
            } else {
                throw new AuthorizationFailure("Данные недоступны!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/salary/{salary}")
    public ResponseEntity findBySalary(@PathVariable Integer salary
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-salary: %s", salary));
        responseStatisticService.updateStat("get-user-by-salary");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> bySalary = userService.getBySalary(salary);
                if (bySalary.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудник с зарплатой %s не найден", salary));
                }
                return ResponseEntity.ok(bySalary);
                } else {
                    throw new AuthorizationFailure("Данные недоступны!");
                }
            } catch(NotFoundException | AuthorizationFailure e){
                throw e;
            } catch(Exception ex){
                throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
            }
    }

    @CrossOrigin("*")
    @GetMapping("/grade/{grade}")
    public ResponseEntity findByGrade(@PathVariable Integer grade
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-grade: %s", grade));
        responseStatisticService.updateStat("get-user-by-grade");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> byGrade = userService.getByGrade(grade);
                if (byGrade.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудник с грейдом %s не найден", grade));
                }
                return ResponseEntity.ok(byGrade);
            } else if (auth.getUserGroup().equals("LAZY_USER")) {
                List<UserNoSalaryDto> byGrade = userService.getByGradeNoSalary(grade);
                if (byGrade.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудник с грейдом %s не найден", grade));
                }
                return ResponseEntity.ok(byGrade);
            } else {
                throw new AuthorizationFailure("Данные недоступны!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
        }
    }

    @CrossOrigin("*")
    @GetMapping("/grade/bound/{lbound}/{ubound}")
    public ResponseEntity findByGradeBetween(@PathVariable Integer lbound, @PathVariable Integer ubound
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie)  {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-grade-bound:l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-grade-bound");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> byGradeBetween = userService.getByGradeBetween(lbound, ubound);
                if (byGradeBetween.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудников в диапазоне зарплат с %s по %s не найдено!", lbound, ubound));
                }
                return ResponseEntity.ok(byGradeBetween);
            } else if (auth.getUserGroup().equals("LAZY_USER")){
                List<UserNoSalaryDto> byGradeBetween = userService.getByGradeBetweenNoSalary(lbound, ubound);
                if (byGradeBetween.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудников в диапазоне зарплат с %s по %s не найдено!", lbound, ubound));
                }
                return ResponseEntity.ok(byGradeBetween);
            } else {
                throw new AuthorizationFailure("Данные недоступны!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("salary/bound/{lbound}/{ubound}")
    public ResponseEntity findBySalaryBetween(@PathVariable Integer lbound, @PathVariable Integer ubound
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-salary-bound:l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-salary-bound");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                List<UserDto> bySalaryBetween = userService.getBySalaryBetween(lbound, ubound);
                if (bySalaryBetween.isEmpty()) {
                    throw new NotFoundException(String.format("Сотрудники с зарплатами в диапазоне с %s по %s не найдены!", lbound, ubound));
                }
                return ResponseEntity.ok(bySalaryBetween);
            } else {
                throw new AuthorizationFailure("Данные недоступны!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с получением данных из базы данных.");
        }

    }

    @CrossOrigin("*")
    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody UserDto userDto, @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("add-user: %s", userDto.getName()));
        responseStatisticService.updateStat("add-user");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                boolean wasAdded = userService.insertUser(userDto);
                if (!wasAdded) {
                    throw new DataBaseUnavailableException("Мы сейчас не можем добавить сотрудника в базу данных.");
                }
                return ResponseEntity.ok(userService.insertUser(userDto));
            } else {
                throw new AuthorizationFailure("Вам недоступно добавление пользователя!");
            }
        } catch (NotFoundException | DataBaseUnavailableException| AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Что-то пошло не так!");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/init")
    public ResponseEntity init(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.warn("init");
        try {
            if (auth.getUserGroup().equals("ADMIN")) {
                UsersFake usersFake = new UsersFake(userRepository);
                usersFake.init();
                ResponseStatisticFake responseStatisticFake = new ResponseStatisticFake(responseStatisticRepository);
                responseStatisticFake.init();
                return ResponseEntity.ok(userService.getAll());
            } else {
                throw new AuthorizationFailure("Вам недоступно добавление пользователей!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности в работе с базой данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users")
    public ResponseEntity<ByteArrayResource> downloadAllUsers(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        List<UserDto> list;
        List<UserNoSalaryDto> listNoSalary;
        ExcelGenerator excelGenerator;
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-all-users-download");
        responseStatisticService.updateStat("get-all-users-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                list = userService.getAll();
                if (list.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(list, UserDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "salary");
            } else if (auth.getUserGroup().equals("LAZY_USER")) {
                listNoSalary = userService.getAllNoSalary();
                if (listNoSalary.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(listNoSalary, UserNoSalaryDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "grade");
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        excelGenerator.setWorkbookName("users_download");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users/demo")
    public ResponseEntity<ByteArrayResource> downloadAllUsersDemoFull(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        List<UserDto> list;
        List<UserNoSalaryDto> listNoSalary;
        ExcelGenerator excelGenerator;
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-all-users-download");
        responseStatisticService.updateStat("get-all-users-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER") || auth.getUserGroup().equals("NONE")) {
                list = userService.getAll();
                if (list.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(list, UserDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "salary");
            } else if (auth.getUserGroup().equals("LAZY_USER")) {
                listNoSalary = userService.getAllNoSalary();
                if (listNoSalary.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(listNoSalary, UserNoSalaryDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "grade");
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        excelGenerator.setWorkbookName("users_download");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users/lazy_demo")
    public ResponseEntity<ByteArrayResource> downloadAllUsersDemoLazy(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        List<UserDto> list;
        List<UserNoSalaryDto> listNoSalary;
        ExcelGenerator excelGenerator;
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-all-users-download");
        responseStatisticService.updateStat("get-all-users-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                list = userService.getAll();
                if (list.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(list, UserDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "salary");
                excelGenerator.setWorkbookName("users_download");
            } else if (auth.getUserGroup().equals("LAZY_USER") || auth.getUserGroup().equals("NONE")) {
                listNoSalary = userService.getAllNoSalary();
                if (listNoSalary.isEmpty()) {
                    throw new NotFoundException("Информация о сотрудниках не найдена!");
                }
                excelGenerator = new ExcelGenerator(listNoSalary, UserNoSalaryDto.class);
                excelGenerator.setChart(ExcelChartEnum.BAR, "name", "grade");
                excelGenerator.setWorkbookName("users_download_lazy");
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        excelGenerator.setWorksheetName("report");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users/grade_bound/{lbound}/{ubound}")
    public ResponseEntity<ByteArrayResource> downloadGradeBound(@PathVariable Integer lbound, @PathVariable Integer ubound,
                                                                @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<UserDto> list;
        List<UserNoSalaryDto> listNoSalary;
        ExcelGenerator excelGenerator;
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-grade-bound-download: l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-grade-bound-download");
       try {
           if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
               list = userService.getByGradeBetween(lbound, ubound);
               if (list.isEmpty()) {
                   throw new NotFoundException(String.format("В диапазоне грейдов с %s по %s ничего не найдено!", lbound, ubound));
               }
               excelGenerator = new ExcelGenerator(list, UserDto.class);
           } else if (auth.getUserGroup().equals("LAZY_USER") || auth.getUserGroup().equals("NONE")){
               listNoSalary = userService.getByGradeBetweenNoSalary(lbound, ubound);
               if (listNoSalary.isEmpty()) {
                   throw new NotFoundException(String.format("В диапазоне грейдов с %s по %s ничего не найдено!", lbound, ubound));
               }
               excelGenerator = new ExcelGenerator(listNoSalary, UserNoSalaryDto.class);
           } else {
               throw new AuthorizationFailure("Вам недоступен данный отчет!");
           }
       } catch (NotFoundException | AuthorizationFailure ex) {
           throw ex;
       } catch (Exception ex) {
           throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
       }

        excelGenerator.setWorkbookName("users_download_grade");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.COL, "name", "grade");
        excelGenerator.setChart3d(ExcelChart3d.ON);
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());
            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users/grade_bound/demo/{lbound}/{ubound}")
    public ResponseEntity<ByteArrayResource> downloadGradeBoundDemo(@PathVariable Integer lbound, @PathVariable Integer ubound,
                                                                @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<UserDto> list;
        List<UserNoSalaryDto> listNoSalary;
        ExcelGenerator excelGenerator;
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-grade-bound-download: l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-grade-bound-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")  || auth.getUserGroup().equals("NONE")) {
                list = userService.getByGradeBetween(lbound, ubound);
                if (list.isEmpty()) {
                    throw new NotFoundException(String.format("В диапазоне грейдов с %s по %s ничего не найдено!", lbound, ubound));
                }
                excelGenerator = new ExcelGenerator(list, UserDto.class);
            } else if (auth.getUserGroup().equals("LAZY_USER")){
                listNoSalary = userService.getByGradeBetweenNoSalary(lbound, ubound);
                if (listNoSalary.isEmpty()) {
                    throw new NotFoundException(String.format("В диапазоне грейдов с %s по %s ничего не найдено!", lbound, ubound));
                }
                excelGenerator = new ExcelGenerator(listNoSalary, UserNoSalaryDto.class);
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        excelGenerator.setWorkbookName("users_download_grade");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.COL, "name", "grade");
        excelGenerator.setChart3d(ExcelChart3d.ON);
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());
            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }


    @CrossOrigin("*")
    @GetMapping("/download/all_users/salary_bound/{lbound}/{ubound}")
    public ResponseEntity<ByteArrayResource> downloadSalaryBound(@PathVariable Integer lbound, @PathVariable Integer ubound,
                                                                 @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<UserDto> list;
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-salary-bound-download: l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-salary-bound-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER")) {
                list = userService.getBySalaryBetween(lbound, ubound);
                if (list.isEmpty()) {
                    throw new NotFoundException(String.format("В диапазоне зарплат с %s по %s ничего не найдено!", lbound, ubound));
                }
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        ExcelGenerator excelGenerator = new ExcelGenerator(list, UserDto.class);
        excelGenerator.setWorkbookName("users_download_salary");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setFormatToVertical("name");
        excelGenerator.setChart(ExcelChartEnum.LINE, "name", "salary");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.BOTTOM);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }

    }

    @CrossOrigin("*")
    @GetMapping("/download/all_users/salary_bound/demo/{lbound}/{ubound}")
    public ResponseEntity<ByteArrayResource> downloadSalaryBoundDemo(@PathVariable Integer lbound, @PathVariable Integer ubound,
                                                                 @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<UserDto> list;
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-user-by-salary-bound-download: l= %s, u= %s", lbound, ubound));
        responseStatisticService.updateStat("get-user-by-salary-bound-download");
        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("USER") || auth.getUserGroup().equals("NONE") ) {
                list = userService.getBySalaryBetween(lbound, ubound);
                if (list.isEmpty()) {
                    throw new NotFoundException(String.format("В диапазоне зарплат с %s по %s ничего не найдено!", lbound, ubound));
                }
            } else {
                throw new AuthorizationFailure("Вам недоступен данный отчет!");
            }
        } catch (NotFoundException | AuthorizationFailure ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        ExcelGenerator excelGenerator = new ExcelGenerator(list, UserDto.class);
        excelGenerator.setWorkbookName("users_download_salary");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setFormatToVertical("name");
        excelGenerator.setChart(ExcelChartEnum.LINE, "name", "salary");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.BOTTOM);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }

    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity NotFoundInDb (NotFoundException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(DataBaseUnavailableException.class)
    public ResponseEntity SQLProblems (DataBaseUnavailableException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

    @ExceptionHandler(ExcelProblemException.class)
    public ResponseEntity ExcelProblems (ExcelProblemException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

    @ExceptionHandler(AuthorizationFailure.class)
    public ResponseEntity BadAuth (AuthorizationFailure e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

}
