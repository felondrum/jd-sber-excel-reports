package ru.sber.jd.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.*;
import ru.sber.jd.dbutils.MediaTypeUtils;
import ru.sber.jd.dto.ClientAuthorizationResultDto;
import ru.sber.jd.dto.ResponseStatisticDto;
import ru.sber.jd.dto.ResponseStatisticPercentDto;
import ru.sber.jd.exceptions.AuthorizationFailure;
import ru.sber.jd.exceptions.DataBaseUnavailableException;
import ru.sber.jd.exceptions.ExcelProblemException;
import ru.sber.jd.exceptions.NotFoundException;
import ru.sber.jd.servicies.AuthService;
import ru.sber.jd.servicies.ResponseStatisticService;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/stats")
public class ResponseStatisticController {
    private final ResponseStatisticService responseStatisticService;
    private final AuthService authService;
    @Autowired
    private ServletContext servletContext;
    @Resource
    @Qualifier(value = "loggerToKafkaResponseStatistic" )
    private LoggerToKafka log;

    @CrossOrigin("*")
    @GetMapping()
    public ResponseEntity findAll(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-all-stats");
        try {
            if (auth.getUserGroup().equals("ADMIN")) {
                List<ResponseStatisticDto> all = responseStatisticService.getAll();
                if (all.isEmpty()) {
                    throw new NotFoundException("Данные со статистикой отсутствуют!");
                }
                return ResponseEntity.ok(all);
            } else {
                throw new AuthorizationFailure("Данные доступны только администратору!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности в работе с базой данных.");
        }
    }

    @CrossOrigin("*")
    @GetMapping("/percent")
    public ResponseEntity findAllByPercent(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("get-stat-by-percent");
        try {
            if (auth.getUserGroup().equals("ADMIN")) {
                List<ResponseStatisticPercentDto> allByPercent = responseStatisticService.getAllByPercent();
                if (allByPercent.isEmpty()) {
                    throw new NotFoundException("Данные со статистикой отсутствуют!");
                }
                return ResponseEntity.ok(allByPercent);
            } else {
                throw new AuthorizationFailure("Данные доступны только администратору!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности в работе с базой данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/{name}")
    public ResponseEntity findByName(@PathVariable String name, @CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info(String.format("get-stat-by-name: %s", name));
        try {
            if (auth.getUserGroup().equals("ADMIN")) {
                List<ResponseStatisticDto> byName = responseStatisticService.getByName(name);
                if (byName.isEmpty()) {
                    throw new NotFoundException(String.format("Данные лога с именем %s отсутствуют", name));
                }
                return ResponseEntity.ok(byName);
            } else {
                throw new AuthorizationFailure("Данные со статистикой отсутствуют!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности в работе с базой данных.");
        }

    }

    @CrossOrigin("*")
    @GetMapping("/download/all_stats")
    public ResponseEntity<ByteArrayResource> downloadAllStats(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("download-all-stats");
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<ResponseStatisticDto> list;

        try {
            if (auth.getUserGroup().equals("ADMIN")) {
                list = responseStatisticService.getAll();
                if (list.isEmpty()) {
                    throw new NotFoundException("Данные со статистикой отсутствуют!");
                }
            } else {
                throw new AuthorizationFailure("Отчет доступен только администратору!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        ExcelGenerator excelGenerator = new ExcelGenerator(list, ResponseStatisticDto.class);
        excelGenerator.setWorkbookName("stats_download");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.PIE, "name", "count");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @CrossOrigin("*")
    @GetMapping("/download/all_stats/demo")
    public ResponseEntity<ByteArrayResource> downloadAllStatsDemo(@CookieValue(value = "authorization_token", required = false) Cookie cookie) {
        ClientAuthorizationResultDto auth = authService.getAuthCheck(cookie);
        log.setAuthor(auth.getLogin());
        log.info("download-all-stats");
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<ResponseStatisticDto> list;

        try {
            if (auth.getUserGroup().equals("ADMIN") || auth.getUserGroup().equals("NONE")) {
                list = responseStatisticService.getAll();
                if (list.isEmpty()) {
                    throw new NotFoundException("Данные со статистикой отсутствуют!");
                }
            } else {
                throw new AuthorizationFailure("Отчет доступен только администратору!");
            }
        } catch (NotFoundException | AuthorizationFailure e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }

        ExcelGenerator excelGenerator = new ExcelGenerator(list, ResponseStatisticDto.class);
        excelGenerator.setWorkbookName("stats_download");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.PIE, "name", "count");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);

        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity NotFoundInDb (NotFoundException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(DataBaseUnavailableException.class)
    public ResponseEntity SQLProblems (DataBaseUnavailableException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

    @ExceptionHandler(ExcelProblemException.class)
    public ResponseEntity ExcelProblems (ExcelProblemException e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

    @ExceptionHandler(AuthorizationFailure.class)
    public ResponseEntity BadAuth (AuthorizationFailure e) {
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

}
