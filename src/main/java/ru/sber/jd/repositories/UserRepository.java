package ru.sber.jd.repositories;

import org.springframework.stereotype.Repository;
import ru.sber.jd.dbutils.DbUtil;
import ru.sber.jd.enities.UserEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Repository
public class UserRepository {
    private final Integer ID_COLUMN = 1;
    private final Integer NAME_COLUMN = 2;
    private final Integer GRADE_COLUMN = 3;
    private final Integer SALARY_COLUMN = 4;

    private final String SELECT_ALL = "select * from users";
    private final String SELECT_BY_NAME = "select * from users where name = ?";
    private final String SELECT_BY_GRADE = "select * from users where grade = ?";
    private final String SELECT_BY_SALARY = "select * from users where salary = ?";
    private final String SELECT_BY_GRADE_BETWEEN = "select * from users where grade between ? and ?";
    private final String SELECT_BY_SALARY_BETWEEN = "select * from users where salary between ? and ?";
    private final String INSERT_USER = "insert into users (id, name, grade, salary) values (?, ?, ?, ?)";

    private final String CREATE_KPI_TABLE = "create table users (id int, name text, grade int, salary int)";
    private final String DROP_KPI_TABLE = "drop table if exists users";
    private final Connection connection = DbUtil.createConnection();

    public void createTable() throws SQLException {
        PreparedStatement dropPreparedStatement = connection.prepareStatement(DROP_KPI_TABLE);
        dropPreparedStatement.execute();
        connection.commit();
        PreparedStatement preparedStatement = connection.prepareStatement(CREATE_KPI_TABLE);
        preparedStatement.execute();
        connection.commit();
    }



    public List<UserEntity> selectAll() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }

    public List<UserEntity> selectByName(String name) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME);
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }

    public List<UserEntity> selectByGrade(Integer grade) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_GRADE);
        preparedStatement.setInt(1, grade);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }

    public List<UserEntity> selectBySalary(Integer salary) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_SALARY);
        preparedStatement.setInt(1, salary);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }

    public List<UserEntity> selectByGradeBetween(Integer gradeLow, Integer gradeHigh) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_GRADE_BETWEEN);
        preparedStatement.setInt(1, gradeLow);
        preparedStatement.setInt(2, gradeHigh);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }

    public List<UserEntity> selectBySalaryBetween(Integer salaryLow, Integer salaryHigh) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_SALARY_BETWEEN);
        preparedStatement.setInt(1, salaryLow);
        preparedStatement.setInt(2, salaryHigh);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<UserEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setGrade(resultSet.getInt(GRADE_COLUMN))
                    .setSalary(resultSet.getInt(SALARY_COLUMN)));
        }
        return entities;
    }


    public void insertUser(UserEntity userEntity) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER);
        preparedStatement.setInt(ID_COLUMN, userEntity.getId());
        preparedStatement.setString(NAME_COLUMN, userEntity.getName());
        preparedStatement.setInt(GRADE_COLUMN, userEntity.getGrade());
        preparedStatement.setInt(SALARY_COLUMN, userEntity.getSalary());
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();

    }


    public void insertUsers(List<UserEntity> usersEntity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER);
        for (UserEntity userEntity:usersEntity) {
            preparedStatement.setInt(ID_COLUMN, userEntity.getId());
            preparedStatement.setString(NAME_COLUMN, userEntity.getName());
            preparedStatement.setInt(GRADE_COLUMN, userEntity.getGrade());
            preparedStatement.setInt(SALARY_COLUMN, userEntity.getSalary());
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();
        connection.commit();
    }
}
