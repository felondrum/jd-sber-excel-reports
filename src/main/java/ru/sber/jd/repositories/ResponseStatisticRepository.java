package ru.sber.jd.repositories;

import org.springframework.stereotype.Repository;
import ru.sber.jd.dbutils.DbUtil;
import ru.sber.jd.dto.ResponseStatisticPercentDto;
import ru.sber.jd.enities.ResponseStatisticEntity;
import ru.sber.jd.enities.UserEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class ResponseStatisticRepository {
    private final Integer ID_COLUMN = 1;
    private final Integer NAME_COLUMN = 2;
    private final Integer COUNT_COLUMN = 3;

    private final String SELECT_ALL = "select * from response_stat";
    private final String SELECT_BY_NAME = "select * from response_stat where name = ?";
    private final String INSERT_STAT = "insert into response_stat (id, name, count) values (?, ?, ?)";
    private final String UPDATE_STAT_BY_NAME = "update response_stat set count = count + 1 where name = ?";
    private final String SELECT_ON_PERCENT = "select a.name" +
            ", cast (case when b.s = 0 then 0 else (a.count/b.s) * 100 end as numeric(20,2)) " +
            "from response_stat a " +
            "cross join (select cast (sum(count) as numeric(20,2)) as s from response_stat) b";

    private final String CREATE_KPI_TABLE = "create table response_stat (id int, name text, count int)";
    private final String DROP_KPI_TABLE = "drop table if exists response_stat";
    private final Connection connection = DbUtil.createConnection();


    public void createTable() throws SQLException {
        PreparedStatement dropPreparedStatement = connection.prepareStatement(DROP_KPI_TABLE);
        dropPreparedStatement.execute();
        connection.commit();
        PreparedStatement preparedStatement = connection.prepareStatement(CREATE_KPI_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    public List<ResponseStatisticEntity> selectAll() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ResponseStatisticEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new ResponseStatisticEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setCount(resultSet.getInt(COUNT_COLUMN)));
        }
        return entities;
    }

    public List<ResponseStatisticPercentDto> selectAllPercent() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ON_PERCENT);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ResponseStatisticPercentDto> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new ResponseStatisticPercentDto()
                    .setName(resultSet.getString(1))
                    .setPercent(resultSet.getDouble(2)));
        }
        return entities;
    }


    public List<ResponseStatisticEntity> selectByName(String name) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME);
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ResponseStatisticEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new ResponseStatisticEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setName(resultSet.getString(NAME_COLUMN))
                    .setCount(resultSet.getInt(COUNT_COLUMN)));
        }
        return entities;
    }

    public void insertStat(ResponseStatisticEntity ResponseStatisticEntity) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STAT);
        preparedStatement.setInt(ID_COLUMN, ResponseStatisticEntity.getId());
        preparedStatement.setString(NAME_COLUMN, ResponseStatisticEntity.getName());
        preparedStatement.setInt(COUNT_COLUMN, ResponseStatisticEntity.getCount());
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();

    }


    public void insertStats(List<ResponseStatisticEntity> ResponseStatisticEntities) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STAT);
        for (ResponseStatisticEntity responseStatisticEntity:ResponseStatisticEntities) {
            preparedStatement.setInt(ID_COLUMN, responseStatisticEntity.getId());
            preparedStatement.setString(NAME_COLUMN, responseStatisticEntity.getName());
            preparedStatement.setInt(COUNT_COLUMN, responseStatisticEntity.getCount());
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();
        connection.commit();
    }

    public void updateStatByName(String name) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STAT_BY_NAME);
        preparedStatement.setString(1, name);
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();

    }

}
