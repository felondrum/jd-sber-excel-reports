package ru.sber.jd.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.sber.jd.ConfigLoggerToKafkaService;
import ru.sber.jd.KafkaAppenderBuilder;
import ru.sber.jd.LoggerComponentCustomBuilder;
import ru.sber.jd.LoggerToKafka;
import ru.sber.jd.controllers.ResponseStatisticController;
import ru.sber.jd.controllers.UserController;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class KafkaLoggerConfiguration {
    @Bean
    public ConfigLoggerToKafkaService executeConfigLog() {

        List<LoggerComponentCustomBuilder> loggerComponentCustomBuilderList = new ArrayList<>();
        loggerComponentCustomBuilderList.add(new LoggerComponentCustomBuilder("ru.sber.jd"));
        List<KafkaAppenderBuilder> kafkaAppenderBuilderList = new ArrayList<>();
        KafkaAppenderBuilder kafkaAppenderBuilder = new KafkaAppenderBuilder();
        //kafkaAppenderBuilder.setPort("37.46.131.116:9092");

        kafkaAppenderBuilder.setPort("localhost:9092");
        kafkaAppenderBuilderList.add(kafkaAppenderBuilder);
        ConfigLoggerToKafkaService configLog = new ConfigLoggerToKafkaService();
        configLog.setLoggersList(loggerComponentCustomBuilderList);
        configLog.setKafkaAppenderList(kafkaAppenderBuilderList);
        configLog.executeConfigForKafka();
        return configLog;
    }

    @Bean
    public LoggerToKafka loggerToKafkaUserController() {
        LoggerToKafka loggerToKafka = new LoggerToKafka();
        loggerToKafka.setProject("Excel Report Application");
        loggerToKafka.setAuthor("Anton Filippov");
        loggerToKafka.setaClass(UserController.class);
        return loggerToKafka;
    }


    @Bean
    public LoggerToKafka loggerToKafkaResponseStatistic() {
        LoggerToKafka loggerToKafka = new LoggerToKafka();
        loggerToKafka.setProject("Excel Statistic Application");
        loggerToKafka.setAuthor("Anton Filippov");
        loggerToKafka.setaClass(ResponseStatisticController.class);
        return loggerToKafka;
    }



}
