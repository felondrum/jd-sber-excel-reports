package ru.sber.jd.exceptions;

public class AuthorizationFailure extends RuntimeException {
    public AuthorizationFailure(String message) {
        super(message);
    }
}
