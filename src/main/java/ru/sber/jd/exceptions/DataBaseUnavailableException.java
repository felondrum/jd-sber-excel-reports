package ru.sber.jd.exceptions;

public class DataBaseUnavailableException extends RuntimeException {
    public DataBaseUnavailableException(String message) {
        super(message);
    }
}
